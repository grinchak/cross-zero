'use strict';

var G_VAR = {
	strokeType: "cross",
	winner: "", // winner name: cross|zero
	fieldSize: 3 // field size, by default 3x3
};

class Start extends React.Component {
	constructor(props) {
		super(props);
		
		this.doParentToggleFromChild = this.doParentToggleFromChild.bind(this);
		this.handleInputChange = this.handleInputChange.bind(this);
	}
	
	/**
	 * Change screen from parent component
	 */
	doParentToggleFromChild(){
		this.props.changeScreen('Field', G_VAR.fieldSize) // show Field screen
	}
	
	handleInputChange(e) {
		G_VAR.fieldSize = e.target.value;
	}
	
	render() {
		G_VAR.strokeType = 'cross'; // set strokeType to default - cross
		G_VAR.fieldSize = 3; // set fieldSize to default - 3
		
		return(
			<div>
				<div>
					<div>Выберите размерность поля</div>
					<p>
						<input type="radio" id="label_3" name="stroke_type" value="3" defaultChecked={true} onClick={this.handleInputChange} />
						<label htmlFor="label_3">3x3</label>
					</p>
					<p>
						<input type="radio" id="label_5" name="stroke_type" value="5" onClick={this.handleInputChange} />
						<label htmlFor="label_5">5x5</label>
					</p>
					<p>
						<input type="radio" id="label_7" name="stroke_type" value="7" onClick={this.handleInputChange} />
						<label htmlFor="label_7">7x7</label>
					</p>
				</div>
				<button onClick={ this.doParentToggleFromChild }>Играть</button>
			</div>
		);
	}
}

class Cell extends React.Component {
	constructor(props) {
		super(props);
		
		this.state = {
			zero_active: '',
			cross_active: '',
			clickDone: false // use additional state clickDone, to avoid unnecessary calls of checkWin function in method componentDidUpdate()
		};
		
		this.clickCell = this.clickCell.bind(this);
		this.checkWin = this.checkWin.bind(this);
		this.updateSettings = this.updateSettings.bind(this);
	}
	
	/**
	 * Method, called, when cell is clicked
	 */
	clickCell(el) {
		// check whether click on .cell element
		if (el.target.classList.contains('cell')) {
			// check if cell not checked
			if ( !el.target.classList.contains('cross_active') && !el.target.classList.contains('zero_active') ) {
				if (G_VAR.strokeType == 'cross') { // current stroke - cross
					this.setState({zero_active: 'cross_active'});
					G_VAR.strokeType = 'zero';
				} else { // current stroke - zero
					this.setState({zero_active: 'zero_active'});
					G_VAR.strokeType = 'cross';
				}
				
				this.setState({
					clickDone: true
				});
			}
		}
	}
	
	/**
	 * Update settings in local storage
	 */
	updateSettings() {
		
		console.log('updateSettings');
		
		var cellPositions = [];
		
		var elements = document.querySelectorAll('.cell');
		
		for (var i = 0; i < elements.length; i++) {
			if( elements[i].classList.contains('cross_active') ) {
				cellPositions.push('x');
			} else if( elements[i].classList.contains('zero_active') ) {
				cellPositions.push('0');
			} else {
				cellPositions.push('');
			}
		}
		
		// set settings to localStorage
		var settings = {
			strokeType: G_VAR.strokeType,
			fieldSize: G_VAR.fieldSize,
			cellPositions: cellPositions
		};
		
		settings = JSON.stringify(settings);
		F.setSettingsLocal({"settings": settings});
		G_VAR.lSettings = settings;
	}
		
	/**
	 * Check win
	 */
	checkWin() {
		
		// -------------------------------
		// Get combination(mask) of
		// appropriate type cells on field
		// -------------------------------
		
		var currCombZero = []; // combination(mask) of zero cells on field
		var currCombCross = []; // combination(mask) of cross cells on field
		var selectedCells = 0; // amount of selected cells
		
		var elements = document.querySelectorAll('.cell');
		
		for (var i = 0; i < elements.length; i++) {
			if( elements[i].classList.contains('zero_active') ) {
				currCombZero.push(1);
				selectedCells++;
			} else {
				currCombZero.push(0);
			}
			
			if( elements[i].classList.contains('cross_active') ) {
				currCombCross.push(1);
				selectedCells++;
			} else {
				currCombCross.push(0);
			}
		}
		
		// --------------------
		// Set needed variables
		// --------------------
		
		var fieldSize = parseInt(G_VAR.fieldSize);
		var winCellAmount;
		
		// set win cell amount, depending on field size
		if(G_VAR.fieldSize == 3) {
			winCellAmount = 3;
		} else {
			winCellAmount = 4;
		}
		
		// -------------------------
		// STEP1: check on cross win
		// -------------------------
		
		var check = F.checkWinMatch(currCombCross, fieldSize, winCellAmount);
		
		if(check) {
			var winCombination = check;
			this.showWinCells(winCombination);
			
			G_VAR.winner = 'cross'; // set winner `Cross`
			
			return false;
		}
		
		// ------------------------
		// STEP2: check on zero win
		// ------------------------
		
		var check = F.checkWinMatch(currCombZero, fieldSize, winCellAmount);
		
		if(check) {
			var winCombination = check;
			this.showWinCells(winCombination);
			
			G_VAR.winner = 'zero'; // set winner `Zero`
			
			return false;
		}
		
		// --------------------
		// STEP3: check on draw
		// --------------------
		
		if( selectedCells == Math.pow(G_VAR.fieldSize, 2) ) {
			G_VAR.winner = 'none'; // set draw (nobody win)
			
			this.props.changeScreen('ModalWin');
			
			return false;
		}
	}
	
	/**
	 * Show win cells
	 */
	showWinCells(comb) {
		var elements = document.querySelectorAll('.cell');
		
		for (var i = 0; i < comb.length; i++) {
			elements[ comb[i] ].className += ' win';
		}
		
		this.props.changeScreen('ModalWin');
	}
	
	// On component update (react lifecycle method)
	componentDidUpdate() {
		if( this.state.clickDone ) {
			this.checkWin();
			this.updateSettings(); // update settings to localStorage
			
			this.setState({
				clickDone: false
			});
		}
	}
	
	render() {
		var classes = [
			'cell',
			this.state.zero_active,
			this.state.cross_active
		].join(' ');
		return(
			<div className={classes} data-id={this.props.id} onClick={this.clickCell.bind(this)}>
				<i className="fa fa-circle-o" aria-hidden="true"></i>
				<i className="fa fa-times" aria-hidden="true"></i>
			</div>
		);
	}
}

class Field extends React.Component {
	constructor(props) {
		super(props);
		
		this.doParentToggleFromChild = this.doParentToggleFromChild.bind(this);
	}
	
	doParentToggleFromChild(){	
		this.props.changeScreen('Start') // show Start screen
	}
	
	componentDidMount() {
		// set game_status = game
		F.setSettingsLocal({"game_status": "game"});
		
		// get settings from localStorage
		var settings = F.getSettingsLocal('settings');
		
		if(settings != null) { // settings exist in localStorage
			settings = JSON.parse(settings);
			var cellPositions = settings.cellPositions;
			
			// set field cells from settings
			var elements = document.querySelectorAll('.cell');
			
			for (var i = 0; i < elements.length; i++) {
				if( cellPositions[i] == 'x' ) {
					elements[i].className += ' cross_active';
				} else if(  cellPositions[i] == '0' ) {
					elements[i].className += ' zero_active';
				}
			}
			
			// set strokeType to G_VAR
			G_VAR.stokeType = settings.strokeType;
		} else { // no settings in localStorage
			var cellPositions = [];
			for(var i = 0; i < Math.pow(G_VAR.fieldSize, 2); i++) {
				cellPositions.push('');
			}
			
			// set settings to localStorage
			var settings = {
				strokeType: "cross",
				fieldSize: G_VAR.fieldSize,
				cellPositions: cellPositions
			};
			
			settings = JSON.stringify(settings);
			F.setSettingsLocal({"game_status": "game", "settings": settings});
		}
	}
	
	render() {
		// get settings from localStorage
		var settings = F.getSettingsLocal('settings');
		
		var fieldSize;
		
		if(settings != null) { // settings exist in localStorage
			settings = JSON.parse(settings);
			fieldSize = settings.fieldSize;
			G_VAR.fieldSize = fieldSize;
			G_VAR.strokeType = settings.strokeType;
		} else {
			fieldSize = G_VAR.fieldSize;
		}
		
		var cells = [];
		
		// make rows
		for (var i = 0; i < fieldSize; i++) {
			// make cols to row
			for (var j = 0; j < fieldSize; j++) {
				cells.push(<Cell id={String(i) + String(j)} key={String(i) + String(j)} changeScreen={this.props.changeScreen} />);
			}
		};
		
		return (
			<div>
				<div style={{width: fieldSize * 40 + fieldSize * 4 + 'px'}} className="field-container">
					{cells}
				</div>
				<div>
					<button onClick={ this.doParentToggleFromChild }>Новая игра</button>
				</div>
			</div>
		);
	}
}

class Main extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			showStart: true,
			showField: false,
			showModalWin: false
		};
		
		this.changeScreen  = this.changeScreen .bind(this);
	}
	
	changeScreen (screen, fieldSize) {
		if(fieldSize !== undefined) {
			G_VAR.fieldSize = fieldSize;
		}
		
		if(screen == 'Field') {
			this.setState({
				showStart: false,
				showField: true,
				showModalWin: false
			});
		} else if(screen == 'Start') {
			// set game_status = start
			F.setSettingsLocal({"game_status": "start"});
			F.removeSettingsLocal('settings');
			
			this.setState({
				showStart: true,
				showField: false,
				showModalWin: false
			});
		} else if(screen == 'ModalWin') {
			this.setState({
				showModalWin: true
			});
		}
	}
	
	componentWillMount() {
		// check game settings from localStorage
		var status = F.getSettingsLocal('game_status');
		
		if(status === null) {
			F.setSettingsLocal({"game_status": "start"});
			F.removeSettingsLocal('settings');
		} else if(status == 'game') {
			this.setState({
				showStart: false,
				showField: true,
				showModalWin: false
			});
		}
	}
	
	render() {
		return (
			<div>
				{this.state.showStart && <Start changeScreen={this.changeScreen} />}
				{this.state.showField && <Field changeScreen={this.changeScreen} />}
				{this.state.showModalWin && <ModalWin changeScreen={this.changeScreen} />}
			</div>
		);
	}
}

class ModalWin extends React.Component {
	constructor(props) {
		super(props);
		
		this.doParentToggleFromChild = this.doParentToggleFromChild.bind(this);
	}
	
	doParentToggleFromChild(){	
		this.props.changeScreen('Start') // show Start screen
	}
	
	render() {
		// set game_status = start
		F.setSettingsLocal({"game_status": "start"});
		F.removeSettingsLocal('settings');
		
		var winnerText = '';
		if(G_VAR.winner == 'cross') {
			winnerText = 'Победили крестики.';
		} else if(G_VAR.winner == 'zero') {
			winnerText = 'Победили нолики.';
		} else if(G_VAR.winner == 'none') {
			winnerText = 'Ничья.';
		}
		
		return(
			<div className="modal">
				<div className="body">
					<div>{winnerText}</div>
					<button onClick={ this.doParentToggleFromChild }>Новая игра</button>
				</div>
			</div>
		);
	}
}

const element = <div><Main /></div>;

ReactDOM.render(
	element,
	document.getElementById('container')
);