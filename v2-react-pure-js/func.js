/**
 * All helper functions are in this file
 */

(function(){
	var F = {};
	
	/**
	 * Check win match
	 *
	 * @param {array} mask - Array of active cells mask
	 * @param {int} fieldSize - Field size
	 * @param {int} winCellAmount - Amount of win cell
	 *
	 * @return boolean|array - Array: win combination cell positions
	 */
	F.checkWinMatch = function(mask, fieldSize, winCellAmount) {
		
		// --------------------------------------------
		// Check
		// Win cell amount should be <= than field size
		// --------------------------------------------
		
		if(winCellAmount > fieldSize) {
			console.error('Make win combination error: win cell size should be less or equal than field size');
			return false;
		}
		
		// --------------------------
		// STEP1: Check on horizontal
		// --------------------------
		
		for(var a = 0; a < fieldSize; a++) {
			for(var i = 0; i <= fieldSize - winCellAmount; i++) {
				var comb = [];
				var check = true;
				for(var j = 0; j < winCellAmount; j++) {
					comb[j] = a * fieldSize + i + j;
					
					// no match, break cycle
					if(mask[ comb[j] ] != 1) {
						check = false;
						break;
					}
				}
				
				// check: win
				if(check) {
					return comb;
				}
			}
		}
		
		// --------------------------
		// STEP2: Check on vertical
		// --------------------------
		
		for(var a = 0; a < fieldSize; a++) {
			for(var i = 0; i <= fieldSize - winCellAmount; i++) {
				var comb = [];
				var check = true;
				for(var j = 0; j < winCellAmount; j++) {
					comb[j] = a + i * fieldSize + j * fieldSize;
					
					// no match, break cycle
					if(mask[ comb[j] ] != 1) {
						check = false;
						break;
					}
				}
				
				// check: win
				if(check) {
					return comb;
				}
			}
		}
		
		// ---------------------------------
		// STEP3: Check on left diagonal `\`
		// ---------------------------------
		
		for(var a = 0; a <= fieldSize - winCellAmount; a++) {
			for(var i = 0; i <= fieldSize - winCellAmount; i++) {
				var comb = [];
				var check = true;
				for(var j = 0; j < winCellAmount; j++) {
					comb[j] = a + i * fieldSize + j * (fieldSize + 1);
					
					// no match, break cycle
					if(mask[ comb[j] ] != 1) {
						check = false;
						break;
					}
				}
				
				// check: win
				if(check) {
					return comb;
				}
			}
		}
		
		// ----------------------------------
		// STEP4: Check on right diagonal `/`
		// ----------------------------------
		
		for(var a = 0; a <= fieldSize - winCellAmount; a++) {
			for(var i = 0; i <= fieldSize - winCellAmount; i++) {
				var comb = [];
				var check = true;
				for(var j = 0; j < winCellAmount; j++) {
					comb[j] = a + i * fieldSize + j * (fieldSize - 1) + (winCellAmount - 1);
					
					// no match, break cycle
					if(mask[ comb[j] ] != 1) {
						check = false;
						break;
					}
				}
				
				// check: win
				if(check) {
					return comb;
				}
			}
		}
		
		// no matches
		return false;
	};
	
	/**
	 * Set game settings to localStorage
	 *
	 * @param {object} data: key - data
	 */
	F.setSettingsLocal = function(data) {
		for(var i in data) {
			localStorage.setItem(i, data[i]);
		}
	}
	
	/**
	 * Set game settings to localStorage
	 *
	 * @param {string} key
	 *
	 * @return mixed
	 */
	F.getSettingsLocal = function(key) {
		var res = localStorage.getItem(key);
		return res;
	}
	
	/**
	 * Remove game settings from localStorage
	 *
	 * @param {string} key
	 */
	F.removeSettingsLocal = function(key) {
		localStorage.removeItem(key);
	}
	
	window.F = F; // export variable to window object
})();