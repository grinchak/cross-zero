/**
 * Test file to work with redux
 */

import { createStore, combineReducers } from 'redux';
 
const userReducer = function(state = {}, action) {
	return state;
};

const widgetReducer = function(state = {}, action) {
	return state;
}

// Combine Reducers
const reducers = combineReducers({
	userState: userReducer,
	widgetState: widgetReducer
});

const store = createStore(reducers);