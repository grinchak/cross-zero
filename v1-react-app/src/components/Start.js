import React from 'react';

import G_VAR from '../variables';

class Start extends React.Component {
	constructor(props) {
		super(props);
		
		this.doParentToggleFromChild = this.doParentToggleFromChild.bind(this);
		this.handleInputChange = this.handleInputChange.bind(this);
	}
	
	/**
	 * Change screen from parent component
	 */
	doParentToggleFromChild(){
		this.props.changeScreen('Field', G_VAR.fieldSize) // show Field screen
	}
	
	handleInputChange(e) {
		G_VAR.fieldSize = e.target.value;
	}
	
	render() {
		G_VAR.strokeType = 'cross'; // set strokeType to default - cross
		G_VAR.fieldSize = 3; // set fieldSize to default - 3
		
		return(
			<div>
				<div>
					<div>Выберите размерность поля</div>
					<p>
						<input type="radio" id="label_3" name="stroke_type" value="3" defaultChecked={true} onClick={this.handleInputChange} />
						<label htmlFor="label_3">3x3</label>
					</p>
					<p>
						<input type="radio" id="label_5" name="stroke_type" value="5" onClick={this.handleInputChange} />
						<label htmlFor="label_5">5x5</label>
					</p>
					<p>
						<input type="radio" id="label_7" name="stroke_type" value="7" onClick={this.handleInputChange} />
						<label htmlFor="label_7">7x7</label>
					</p>
				</div>
				<button onClick={ this.doParentToggleFromChild }>Играть</button>
			</div>
		);
	}
}

export default Start;