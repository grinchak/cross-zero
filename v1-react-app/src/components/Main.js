import React from 'react';

import G_VAR from '../variables';
import F from '../functions';

import Start from './Start';
import Field from './Field';
import ModalWin from './ModalWin';

class Main extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			showStart: true,
			showField: false,
			showModalWin: false
		};
		
		this.changeScreen  = this.changeScreen.bind(this);
	}
	
	changeScreen (screen, fieldSize) {
		if(fieldSize !== undefined) {
			G_VAR.fieldSize = fieldSize;
		}
		
		if(screen === 'Field') {
			this.setState({
				showStart: false,
				showField: true,
				showModalWin: false
			});
		} else if(screen === 'Start') {
			// set game_status = start
			F.setSettingsLocal({"game_status": "start"});
			F.removeSettingsLocal('settings');
			
			this.setState({
				showStart: true,
				showField: false,
				showModalWin: false
			});
		} else if(screen === 'ModalWin') {
			this.setState({
				showModalWin: true
			});
		}
	}
	
	componentWillMount() {
		// check game settings from localStorage
		let status = F.getSettingsLocal('game_status');
		
		if(status === null) {
			F.setSettingsLocal({"game_status": "start"});
			F.removeSettingsLocal('settings');
		} else if(status === 'game') {
			this.setState({
				showStart: false,
				showField: true,
				showModalWin: false
			});
		}
	}
	
	render() {
		return (
			<div>
				{this.state.showStart && <Start changeScreen={this.changeScreen} />}
				{this.state.showField && <Field changeScreen={this.changeScreen} />}
				{this.state.showModalWin && <ModalWin changeScreen={this.changeScreen} />}
			</div>
		);
	}
}

export default Main;