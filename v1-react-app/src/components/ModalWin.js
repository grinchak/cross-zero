import React from 'react';

import G_VAR from '../variables';
import F from '../functions';

class ModalWin extends React.Component {
	constructor(props) {
		super(props);
		
		this.doParentToggleFromChild = this.doParentToggleFromChild.bind(this);
	}
	
	doParentToggleFromChild(){	
		this.props.changeScreen('Start') // show Start screen
	}
	
	render() {
		// set game_status = start
		F.setSettingsLocal({"game_status": "start"});
		F.removeSettingsLocal('settings');
		
		var winnerText = '';
		if(G_VAR.winner === 'cross') {
			winnerText = 'Победили крестики.';
		} else if(G_VAR.winner === 'zero') {
			winnerText = 'Победили нолики.';
		} else if(G_VAR.winner === 'none') {
			winnerText = 'Ничья.';
		}
		
		return(
			<div className="modal">
				<div className="body">
					<div>{winnerText}</div>
					<button onClick={ this.doParentToggleFromChild }>Новая игра</button>
				</div>
			</div>
		);
	}
}

export default ModalWin;