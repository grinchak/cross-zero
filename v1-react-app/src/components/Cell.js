import React from 'react';

import G_VAR from '../variables';
import F from '../functions';

class Cell extends React.Component {
	constructor(props) {
		super(props);
		
		this.state = {
			zero_active: '',
			cross_active: '',
			clickDone: false // use additional state clickDone, to avoid unnecessary calls of checkWin function in method componentDidUpdate()
		};
		
		this.clickCell = this.clickCell.bind(this);
		this.checkWin = this.checkWin.bind(this);
		this.updateSettings = this.updateSettings.bind(this);
	}
	
	/**
	 * Method, called, when cell is clicked
	 */
	clickCell(el) {
		// check whether click on .cell element
		if (el.target.classList.contains('cell')) {
			// check if cell not checked
			if ( !el.target.classList.contains('cross_active') && !el.target.classList.contains('zero_active') ) {
				if (G_VAR.strokeType === 'cross') { // current stroke - cross
					this.setState({zero_active: 'cross_active'});
					G_VAR.strokeType = 'zero';
				} else { // current stroke - zero
					this.setState({zero_active: 'zero_active'});
					G_VAR.strokeType = 'cross';
				}
				
				this.setState({
					clickDone: true
				});
			}
		}
	}
	
	/**
	 * Update settings in local storage
	 */
	updateSettings() {
		var cellPositions = [];
		
		var elements = document.querySelectorAll('.cell');
		
		for (var i = 0; i < elements.length; i++) {
			if( elements[i].classList.contains('cross_active') ) {
				cellPositions.push('x');
			} else if( elements[i].classList.contains('zero_active') ) {
				cellPositions.push('0');
			} else {
				cellPositions.push('');
			}
		}
		
		// set settings to localStorage
		var settings = {
			strokeType: G_VAR.strokeType,
			fieldSize: G_VAR.fieldSize,
			cellPositions: cellPositions
		};
		
		settings = JSON.stringify(settings);
		F.setSettingsLocal({"settings": settings});
		G_VAR.lSettings = settings;
	}
		
	/**
	 * Check win
	 */
	checkWin() {
		
		// -------------------------------
		// Get combination(mask) of
		// appropriate type cells on field
		// -------------------------------
		
		var currCombZero = []; // combination(mask) of zero cells on field
		var currCombCross = []; // combination(mask) of cross cells on field
		var selectedCells = 0; // amount of selected cells
		
		var elements = document.querySelectorAll('.cell');
		
		for (var i = 0; i < elements.length; i++) {
			if( elements[i].classList.contains('zero_active') ) {
				currCombZero.push(1);
				selectedCells++;
			} else {
				currCombZero.push(0);
			}
			
			if( elements[i].classList.contains('cross_active') ) {
				currCombCross.push(1);
				selectedCells++;
			} else {
				currCombCross.push(0);
			}
		}
		
		// --------------------
		// Set needed variables
		// --------------------
		
		var fieldSize = parseInt(G_VAR.fieldSize, 10);
		var winCellAmount;
		
		// set win cell amount, depending on field size
		if(G_VAR.fieldSize === 3) {
			winCellAmount = 3;
		} else {
			winCellAmount = 4;
		}
		
		// -------------------------
		// STEP1: check on cross win
		// -------------------------
		
		let check;
		let winCombination;
		
		check = F.checkWinMatch(currCombCross, fieldSize, winCellAmount);
		
		if(check) {
			winCombination = check;
			this.showWinCells(winCombination);
			
			G_VAR.winner = 'cross'; // set winner `Cross`
			
			return false;
		}
		
		// ------------------------
		// STEP2: check on zero win
		// ------------------------
		
		check = F.checkWinMatch(currCombZero, fieldSize, winCellAmount);
		
		if(check) {
			winCombination = check;
			this.showWinCells(winCombination);
			
			G_VAR.winner = 'zero'; // set winner `Zero`
			
			return false;
		}
		
		// --------------------
		// STEP3: check on draw
		// --------------------
		
		if( selectedCells === Math.pow(G_VAR.fieldSize, 2) ) {
			G_VAR.winner = 'none'; // set draw (nobody win)
			
			this.props.changeScreen('ModalWin');
			
			return false;
		}
	}
	
	/**
	 * Show win cells
	 */
	showWinCells(comb) {
		let elements = document.querySelectorAll('.cell');
		
		for (let i = 0; i < comb.length; i++) {
			elements[ comb[i] ].className += ' win';
		}
		
		this.props.changeScreen('ModalWin');
	}
	
	// On component update (react lifecycle method)
	componentDidUpdate() {
		if( this.state.clickDone ) {
			this.checkWin();
			this.updateSettings(); // update settings to localStorage
			
			this.setState({
				clickDone: false
			});
		}
	}
	
	render() {
		let classes = [
			'cell',
			this.state.zero_active,
			this.state.cross_active
		].join(' ');
		return(
			<div className={classes} data-id={this.props.id} onClick={this.clickCell.bind(this)}>
				<i className="fa fa-circle-o" aria-hidden="true"></i>
				<i className="fa fa-times" aria-hidden="true"></i>
			</div>
		);
	}
}

export default Cell;