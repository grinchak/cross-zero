import React from 'react';

import G_VAR from '../variables';
import F from '../functions';

import Cell from './Cell';

class Field extends React.Component {
	constructor(props) {
		super(props);
		
		this.doParentToggleFromChild = this.doParentToggleFromChild.bind(this);
	}
	
	doParentToggleFromChild(){	
		this.props.changeScreen('Start') // show Start screen
	}
	
	componentDidMount() {
		// set game_status = game
		F.setSettingsLocal({"game_status": "game"});
		
		// get settings from localStorage
		let settings = F.getSettingsLocal('settings');
		
		let cellPositions;
		let i;
		
		if(settings != null) { // settings exist in localStorage
			settings = JSON.parse(settings);
			cellPositions = settings.cellPositions;
			
			// set field cells from settings
			let elements = document.querySelectorAll('.cell');
			
			for (i = 0; i < elements.length; i++) {
				if( cellPositions[i] === 'x' ) {
					elements[i].className += ' cross_active';
				} else if(  cellPositions[i] === '0' ) {
					elements[i].className += ' zero_active';
				}
			}
			
			// set strokeType to G_VAR
			G_VAR.stokeType = settings.strokeType;
		} else { // no settings in localStorage
			cellPositions = [];
			for(i = 0; i < Math.pow(G_VAR.fieldSize, 2); i++) {
				cellPositions.push('');
			}
			
			// set settings to localStorage
			settings = {
				strokeType: "cross",
				fieldSize: G_VAR.fieldSize,
				cellPositions: cellPositions
			};
			
			settings = JSON.stringify(settings);
			F.setSettingsLocal({"game_status": "game", "settings": settings});
		}
	}
	
	render() {
		// get settings from localStorage
		let settings = F.getSettingsLocal('settings');
		
		let fieldSize;
		
		if(settings != null) { // settings exist in localStorage
			settings = JSON.parse(settings);
			fieldSize = settings.fieldSize;
			G_VAR.fieldSize = fieldSize;
			G_VAR.strokeType = settings.strokeType;
		} else {
			fieldSize = G_VAR.fieldSize;
		}
		
		let cells = [];
		
		// make rows
		for (let i = 0; i < fieldSize; i++) {
			// make cols to row
			for (let j = 0; j < fieldSize; j++) {
				cells.push(<Cell id={String(i) + String(j)} key={String(i) + String(j)} changeScreen={this.props.changeScreen} />);
			}
		};
		
		return (
			<div>
				<div style={{width: fieldSize * 40 + fieldSize * 4 + 'px'}} className="field-container">
					{cells}
				</div>
				<div>
					<button onClick={ this.doParentToggleFromChild }>Новая игра</button>
				</div>
			</div>
		);
	}
}

export default Field;