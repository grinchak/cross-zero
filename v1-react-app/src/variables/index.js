let G_VAR = {
	strokeType: "cross",
	winner: "", // winner name: cross|zero
	fieldSize: 3 // field size, by default 3x3
}

export default G_VAR;