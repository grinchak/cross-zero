import React, { Component } from 'react';

import './lib/font-awesome-4.7.0/css/font-awesome.min.css';
import './style.css';

import Main from './components/Main';

class App extends Component {
  render() {
    return (
		<div>
			<Main />
		</div>
    );
  }
}

export default App;
